﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace Flat_Icon_Quiz
{
    public partial class about_page : PhoneApplicationPage
    {
        public about_page()
        {
            InitializeComponent();
            SystemTray.SetOpacity(this, 0.01);
            // A Fun game to challenge your brain, guess what icon stand for. Share your score to your favourite social media.

            // All Icon used in quiz, designed by RoundIcons.com. support RoundIcons.com by purchase their icon.
        }

        private void HyperlinkButton_Click_1(object sender, RoutedEventArgs e)
        {
            EmailComposeTask emailTask = new EmailComposeTask();
            emailTask.To = "bagus@outlook.com";
            emailTask.Subject = "Flat Icon Quiz";
            emailTask.Show();
        }

        private void HyperlinkButton_Click_2(object sender, RoutedEventArgs e)
        {
            MarketplaceReviewTask reviewTask = new MarketplaceReviewTask();
            reviewTask.Show();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flat_Icon_Quiz
{
    public class Level
    {
        public string key { set; get; }
        public string level_name { get; set; }
        public List<Quiz> quizes { set; get; }
    }
}

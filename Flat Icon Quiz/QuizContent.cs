﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flat_Icon_Quiz
{
    class QuizContent
    {
        private Level level1, level2, level3, level4;
        public QuizContent()
        {
            initLevel1();
            initLevel2();
            initLevel3();
            initLevel4();
        }

        public Level getLevel(string level)
        {
            if(level.Equals("level1"))
            {
                return getLevel1();
            }
            else if (level.Equals("level2"))
            {
                return getLevel2();
            }
            else if (level.Equals("level3"))
            {
                return getLevel3();
            }
            else if (level.Equals("level4"))
            {
                return getLevel4();
            }

            return new Level();
        }

        public Level getLevel1()
        {
            return level1;
        }

        public Level getLevel2()
        {
            return level2;
        }

        public Level getLevel3()
        {
            return level3;
        }

        public Level getLevel4()
        {
            return level4;
        }

        private void initLevel4()
        {
            level4 = new Level();
            level4.key = "level4";
            level4.level_name = "Level 4";
            List<Quiz> tmp_quiz = new List<Quiz>();

            List<string> ans = new List<string>()
            {
                "bonfire",
                "coins",
                "female",
                "firework",
                "love",
                "notebook",
                "propeller",
                "school-bus",
                "snowman",
                "tent",
                "usb"
            };

            int i = 1;
            foreach (var item in ans)
            {
                tmp_quiz.Add(new Quiz()
                {
                    id = i,
                    answer = item.Replace("-", " "), 
                    resource_file = "QuizSrc/Level4/free-60-icons-" + item + ".png"
                });
                i++;
            }
            level4.quizes = tmp_quiz;
            
        }

        private void initLevel3()
        {
            level3 = new Level();
            level3.level_name = "Level 3";
            level3.key = "level3";
            List<Quiz> tmp_quiz = new List<Quiz>();

            List<string> ans = new List<string>()
            {
                "bookmark",
                "brush",
                "calendar",
                "chicken",
                "guitar",
                "house",
                "luggage",
                "microscope",
                "papers",
                "recycle"
            };

            int i = 1;
            foreach (var item in ans)
            {
                tmp_quiz.Add(new Quiz()
                {
                    id = i,
                    answer = item.Replace("-", " "), 
                    resource_file = "QuizSrc/Level3/free-60-icons-" + item + ".png"
                });
                i++;
            }
            level3.quizes = tmp_quiz;
        }

        private void initLevel2()
        {
            level2 = new Level();
            level2.level_name = "Level 2";
            level2.key = "level2";
            List<Quiz> tmp_quiz = new List<Quiz>();
            List<string> ans = new List<string>()
            {
                "bike",
                "bomb",
                "bowling",
                "clock",
                "deer",
                "donut",
                "goalpost",
                "plug",
                "polar-bear",
                "xbox"
            };

            int i = 1;
            foreach (var item in ans)
            {
                tmp_quiz.Add(new Quiz() { 
                    id = i, 
                    answer = item.Replace("-", " "), 
                    resource_file = "QuizSrc/Level2/free-60-icons-"+item+".png" 
                });
                i++;
            }
            level2.quizes = tmp_quiz;
        }

        private void initLevel1()
        {
            level1 = new Level();
            level1.level_name = "Level 1";
            level1.key = "level1";

            level1.quizes = new List<Quiz>();

            level1.quizes.Add(new Quiz() { 
                id = 1, 
                answer = "america", 
                resource_file = "QuizSrc/Level1/free-60-icons-america.png" 
            });

            level1.quizes.Add(new Quiz()
            {
                id = 2,
                answer = "butterfly",
                resource_file = "QuizSrc/Level1/free-60-icons-butterfly.png"
            });

            level1.quizes.Add(new Quiz()
            {
                id = 3,
                answer = "calculator",
                resource_file = "QuizSrc/Level1/free-60-icons-calculator.png"
            });

            level1.quizes.Add(new Quiz()
            {
                id = 4,
                answer = "clown",
                resource_file = "QuizSrc/Level1/free-60-icons-clown.png"
            });

            level1.quizes.Add(new Quiz()
            {
                id = 5,
                answer = "football",
                resource_file = "QuizSrc/Level1/free-60-icons-football.png"
            });

            level1.quizes.Add(new Quiz()
            {
                id = 6,
                answer = "germany",
                resource_file = "QuizSrc/Level1/free-60-icons-germany.png"
            });

            level1.quizes.Add(new Quiz()
            {
                id = 7,
                answer = "imac",
                resource_file = "QuizSrc/Level1/free-60-icons-imac.png"
            });

            level1.quizes.Add(new Quiz()
            {
                id = 8,
                answer = "ipad",
                resource_file = "QuizSrc/Level1/free-60-icons-ipad.png"
            });

            level1.quizes.Add(new Quiz()
            {
                id = 9,
                answer = "pizza",
                resource_file = "QuizSrc/Level1/free-60-icons-pizza.png"
            });

            level1.quizes.Add(new Quiz()
            {
                id = 10,
                answer = "umbrella",
                resource_file = "QuizSrc/Level1/free-60-icons-umbrella.png"
            });
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using Microsoft.Phone.Tasks;

namespace Flat_Icon_Quiz
{
    public partial class stats : PhoneApplicationPage
    {
        public stats()
        {
            InitializeComponent();
            SystemTray.SetOpacity(this, 0.01);
            var iso = new IsoHelper();
            Profile profil = JsonConvert.DeserializeObject<Profile>(iso.read("profile.json"));
            solved.Text = profil.solved_quizes.Count().ToString();
            unsolved.Text = (41 - profil.solved_quizes.Count()).ToString();
        }

        private void ApplicationBarIconButton_Click_1(object sender, EventArgs e)
        {
            ShareLinkTask shareLinkTask = new ShareLinkTask();

            shareLinkTask.Title = "Yeah I just solved " + solved.Text + " icon puzzles in Flat Icon Quiz";
            shareLinkTask.LinkUri = new Uri("http://bguzryanto.net", UriKind.Absolute);
            shareLinkTask.Message = "Yeah I just solved " + solved.Text + " icon puzzles in Flat Icon Quiz";

            shareLinkTask.Show();

        }
    }
}
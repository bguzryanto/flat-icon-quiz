﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flat_Icon_Quiz
{
    public class Quiz
    {
        public int id { set; get; }
        public string resource_file { set; get; }
        public string answer { set; get; }
        public Boolean isSolved { set; get; }
        public string check_file { set; get; }
    }
}
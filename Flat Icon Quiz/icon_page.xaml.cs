﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;

namespace Flat_Icon_Quiz
{
    public partial class icon_page : PhoneApplicationPage
    {
        private Level level;
        private string level_name = "";
        private string id;
        private QuizContent quizContent;
        private Quiz quiz;
        private string answer = "";
        public icon_page()
        {
            InitializeComponent();
            SystemTray.SetOpacity(this, 0.01);

            Loaded += (s, e) =>
            {
                quizContent = new QuizContent();
                level = quizContent.getLevel(level_name);
                level_text.Text = level.level_name;
                
                quiz = level.quizes[int.Parse(id)-1];
                answer = quiz.answer;
                guess_image.Source = new BitmapImage(new Uri(quiz.resource_file, UriKind.Relative));
            };
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            NavigationContext.QueryString.TryGetValue("level", out level_name);
            NavigationContext.QueryString.TryGetValue("id", out id);
            //if (NavigationContext.QueryString.TryGetValue("level", out level_name))
            //{
            //    loadPage();
            //}
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if(answerBox.Text.Equals(answer))
            {
                QuizHelper qh = new QuizHelper();
                qh.solved(quiz);
                MessageBox.Show("Success, Your answer is true!");
                NavigationService.GoBack();
            }
            else{
                MessageBox.Show("Sorry, your answer apparently wrong");
            }
        }
    }
}
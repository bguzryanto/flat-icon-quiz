﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flat_Icon_Quiz
{
    class Profile
    {
        public int solved_count { set; get; }
        public QuizContent quizContent { set; get; }
        public List<string> solved_quizes { set; get; }
    }
}

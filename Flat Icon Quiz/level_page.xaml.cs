﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Telerik.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Globalization;
using System.Windows.Data;
using Newtonsoft.Json;

namespace Flat_Icon_Quiz
{
    public partial class level_page : PhoneApplicationPage
    {
        private Level level;
        private string level_name = "";
        private QuizContent quizContent;
        private List<bool> solved_ans;
        public level_page()
        {
            InitializeComponent();
            SystemTray.SetOpacity(this, 0.01);



            Loaded += (s,e) => {
                NavigationContext.QueryString.TryGetValue("level", out level_name);
                quizContent = new QuizContent();
                level = quizContent.getLevel(level_name);
                level_text.Text = level.level_name;

                solved_ans = new List<bool>();
                var iso = new IsoHelper();
                try
                {
                    // koding untuk flag sudah diselesaikan
                    Profile profil = JsonConvert.DeserializeObject<Profile>(iso.read("profile.json"));

                    bool result;
                    foreach (var item in level.quizes)
                    {
                        result = false;
                        foreach (var item2 in profil.solved_quizes)
                        {
                            if (item.resource_file.Equals(item2))
                            {
                                result = true;
                                break;
                            }
                        }
                        solved_ans.Add(result);
                    }

                    // new quiz
                    List<Quiz> quizes = new List<Quiz>();
                    int i = 0;
                    foreach (var item in level.quizes)
                    {
                        if (solved_ans[i])
                        {
                            level.quizes[i].isSolved = true;
                            level.quizes[i].check_file = "/Assets/Check.png";
                        }
                        else
                        {
                            level.quizes[i].isSolved = true;
                            level.quizes[i].check_file = "/Assets/trans.png";
                        }
                        i++;
                    }

                    radListBox.ItemsSource = level.quizes;
                }
                catch (Exception)
                {
                }

                //int i = 0;
                //foreach (var item in solved_ans)
                //{

                //}

            };
        }

        private void im_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Image im = (Image)sender;
        }

        public sealed class ImageConverter : IValueConverter
        {
            public object Convert(object value, Type targetType,
                                  object parameter, CultureInfo culture)
            {
                try
                {
                    return new BitmapImage(new Uri((string)value));
                }
                catch
                {
                    return new BitmapImage();
                }
            }

            public object ConvertBack(object value, Type targetType,
                                      object parameter, CultureInfo culture)
            {
                throw new NotImplementedException();
            }
        }

        private void radListBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Quiz quiz = (Quiz)radListBox.SelectedItem;

                int i = 0;
                foreach (Quiz item in radListBox.RealizedItems)
                {
                    if (item.resource_file.Equals(quiz.resource_file))
                    {
                        break;
                    }
                    i++;
                }

                if (solved_ans[i])
                {
                    MessageBox.Show("You have been answer correctly this icon");
                }
                else
                {
                    NavigationService.Navigate(new Uri("/icon_page.xaml?level=" + level_name + "&id=" + quiz.id, UriKind.Relative));
                }
            }
            catch (Exception)
            {
                
            }
        }

    }
}
﻿using MSPToolkit.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flat_Icon_Quiz
{
    class QuizHelper
    {
        private IsoHelper iso;
        public Profile profile;
        public QuizHelper()
        {
            iso = new IsoHelper();
            string profil = iso.read("profile.json");
            try
            {
                profile = JsonConvert.DeserializeObject<Profile>(profil);
            }
            catch (Exception)
            {
                profile = new Profile()
                {
                    solved_count = 0,
                    solved_quizes = new List<string>(),
                    quizContent = new QuizContent(),
                };
            }
            
        }

        public void solved(Quiz quiz){
            if (profile == null)
                System.Diagnostics.Debug.WriteLine("Variable Profile ERROR");


            System.Diagnostics.Debug.WriteLine("BERHASIL");
            profile.solved_quizes.Add(quiz.resource_file);
            profile.solved_count += 1;
            iso.write("profile.json", JsonConvert.SerializeObject(profile));
        }

        public string getProfil(){
            return JsonConvert.SerializeObject(profile);
        }


        public bool isSolved(Quiz quiz)
        {
            bool ans = false;
            foreach (var item in profile.solved_quizes)
            {
                if (quiz.resource_file.Equals(item))
                {
                    ans = true;
                    break;
                }
            }
            return ans;
        }
        
        //public void loadProfile(){
        //    try
        //    {
        //        System.Diagnostics.Debug.WriteLine(1);
        //        string profil = IsolatedStorageHelper.LoadSerializableObject<String>("profile.json");
        //        System.Diagnostics.Debug.WriteLine(2);
        //        System.Diagnostics.Debug.WriteLine(profil);
        //        profile = JsonConvert.DeserializeObject<Profile>(profil);
        //        System.Diagnostics.Debug.WriteLine(3);
        //        System.Diagnostics.Debug.WriteLine("file ada isinya : " + profil);
        //    }
        //    catch (System.IO.FileNotFoundException)
        //    {
        //        profile = new Profile()
        //        {
        //            quizContent = new QuizContent(),
        //            solved_count = 0,
        //            solved_quizes = new List<string>()
        //        };
        //        string profil = JsonConvert.SerializeObject(profile);
        //        System.Diagnostics.Debug.WriteLine("---------");
        //        System.Diagnostics.Debug.WriteLine(profil);
        //        System.Diagnostics.Debug.WriteLine("---------");
        //        IsolatedStorageHelper.SaveSerializableObject<string>(profil, "profile.json");
        //        System.Diagnostics.Debug.WriteLine("File gak ada");
        //    }
        //    catch(System.Xml.XmlException ex)
        //    {
        //        profile = new Profile()
        //        {
        //            quizContent = new QuizContent(),
        //            solved_count = 0,
        //            solved_quizes = new List<string>()
        //        };
        //        string profil = JsonConvert.SerializeObject(profile);
        //        System.Diagnostics.Debug.WriteLine("---------");
        //        System.Diagnostics.Debug.WriteLine(profil);
        //        System.Diagnostics.Debug.WriteLine("---------");
        //        IsolatedStorageHelper.SaveSerializableObject<string>(profil, "profile.json");
        //        System.Diagnostics.Debug.WriteLine("File corrupt yuk overwrite");
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //    }
        //}

        //public int getSolvedCount()
        //{
        //}

        //public int getUnSolvedCount()
        //{
         
        //}


    }
}

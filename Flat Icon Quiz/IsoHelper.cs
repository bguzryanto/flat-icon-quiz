﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flat_Icon_Quiz
{
    class IsoHelper
    {
        public void write(string filename, string content)
        {
            try
            {
                IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
                StreamWriter writeFile;
                if (!myIsolatedStorage.FileExists(filename))
                {
                    writeFile = new StreamWriter(new IsolatedStorageFileStream(filename, FileMode.OpenOrCreate, myIsolatedStorage));
                    writeFile.WriteLine(content);
                    writeFile.Close();
                }
                else
                {
                    writeFile = new StreamWriter(new IsolatedStorageFileStream(filename, FileMode.Open, myIsolatedStorage));
                    writeFile.WriteLine(content);
                    writeFile.Close();
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }

        public string read(string filename)
        {
            string r = "";
            try
            {

                IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
                if (myIsolatedStorage.FileExists(filename))
                {
                    IsolatedStorageFileStream fileStream = myIsolatedStorage.OpenFile(filename, FileMode.Open, FileAccess.Read);
                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        r = reader.ReadLine();
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
            return r;
        }
    }
}

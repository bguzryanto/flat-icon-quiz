﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Flat_Icon_Quiz.Resources;
using MSPToolkit.Utilities;
using Newtonsoft.Json;
using Microsoft.Phone.Tasks;

namespace Flat_Icon_Quiz
{
    public partial class MainPage : PhoneApplicationPage
    {
        private Profile profile;
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            SystemTray.SetOpacity(this, 0.01);

            Loaded += (s,e) =>
            {
                var iso = new IsoHelper();
                string profil = iso.read("profile.json");
                if (profil.Equals(""))
                {
                    iso.write("profile.json", JsonConvert.SerializeObject(new Profile()
                    {
                        quizContent = new QuizContent(),
                        solved_count = 0,
                        solved_quizes = new List<string>()
                    }));
                    profil = iso.read("profile.json");
                }

                QuizHelper qh = new QuizHelper();
                System.Diagnostics.Debug.WriteLine(qh.getProfil());
            };


            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void StackPanel_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/level_page.xaml?level=level1", UriKind.Relative));
        }

        private void StackPanel_Tap_2(object sender, System.Windows.Input.GestureEventArgs e)
        {

            NavigationService.Navigate(new Uri("/level_page.xaml?level=level2", UriKind.Relative));
        }

        private void StackPanel_Tap_3(object sender, System.Windows.Input.GestureEventArgs e)
        {

            NavigationService.Navigate(new Uri("/level_page.xaml?level=level3", UriKind.Relative));
        }

        private void StackPanel_Tap_4(object sender, System.Windows.Input.GestureEventArgs e)
        {

            NavigationService.Navigate(new Uri("/level_page.xaml?level=level4", UriKind.Relative));
        }

        private void StackPanel_Tap_5(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MessageBox.Show("Coming Soon", "Information", MessageBoxButton.OK);
        }

        private void ApplicationBarIconButton_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/about_page.xaml", UriKind.Relative));
        }

        private void ApplicationBarIconButton_Click_2(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/stats.xaml", UriKind.Relative));
        }

        private void ApplicationBarIconButton_Click_3(object sender, EventArgs e)
        {
            MarketplaceReviewTask reviewTask = new MarketplaceReviewTask();
            reviewTask.Show();
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}